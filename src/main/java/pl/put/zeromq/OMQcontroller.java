package pl.put.zeromq;

import org.springframework.web.bind.annotation.*;

import org.zeromq.SocketType;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZContext;

@RestController
@RequestMapping(path="/zadanie")
public class OMQcontroller
{

	@GetMapping(path="/1")
	public @ResponseBody String zadanie1() throws Exception {	
		String reply = "";		
		try (ZContext context = new ZContext()) {
			
			Socket requester = context.createSocket(SocketType.REQ);	
			requester.connect("tcp://unixlab.cs.put.poznan.pl:5555");

		    	System.out.println("Connected, waiting for reponse...");
		    
		    	for(int counter = 0; counter < 3; counter++){
			    requester.send("131793", 0);
			    reply = requester.recvStr(0);
			    System.out.println(
				    "Received reply (" + counter + "): " + reply
			    );
			    Thread.sleep(200);
		     	}
		    
		} catch(Exception e)
		{ 
			System.out.println("Error : " + e.toString()); 
		}
		return reply;	
    	}
	
	@GetMapping(path="/2")
	public @ResponseBody String zadanie2() throws Exception {
		String address = "tcp://unixlab.cs.put.poznan.pl:6005", address_ = "", contents = "";
		try (ZContext context = new ZContext()) {
			Socket subscriber = context.createSocket(SocketType.SUB);
		
            		subscriber.connect(address);
            		subscriber.subscribe("B".getBytes(ZMQ.CHARSET));

            		while (!Thread.currentThread().isInterrupted()) {
                	// Read envelope with address
                	address_ = subscriber.recvStr();
                	// Read message contents
                	contents = subscriber.recvStr();
                	System.out.println(address_ + " : " + contents);
            		}
        	} catch(Exception e)
		{ 
			System.out.println("Error : " + e.toString()); 
		}
		return address_ + " : " + contents;

	}
	
	
	@GetMapping(path="/3")
	public @ResponseBody String zadanie3() throws Exception {
		try (ZContext context = new ZContext()) {
            		ZMQ.Socket sender = context.createSocket(SocketType.PUSH);
            		sender.connect("tcp://unixlab.cs.put.poznan.pl:6001");

		    	for(int counter = 0; counter < 3; counter++){
			    long start = System.currentTimeMillis();
				
			    sender.send("maciejlaszkiewicz",0);
				
			    long finish = System.currentTimeMillis();
			    long timeElapsed = finish - start;
			    System.out.println(counter + ": " + timeElapsed + " ms");
			    Thread.sleep(200);
		     	}
        	} catch(Exception e)
		{ 
			System.out.println("Error : " + e.toString()); 
		}
		return "";
	}
}
